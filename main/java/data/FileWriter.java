package data;

import java.io.File;
import java.io.IOException;

public abstract class FileWriter {
    private static int reportNo;
    private static int billNo;

    public static void writeReport(String text) {
        reportNo++;
        File file = new File("src\\main\\resources\\Report" + reportNo + ".txt");
        try {
            java.io.FileWriter myWriter = new java.io.FileWriter(file);
            myWriter.write(text);
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    public static void writeBill(String text) {
        billNo++;
        File file = new File("src\\main\\resources\\Bill" + billNo + ".txt");
        try {
            java.io.FileWriter myWriter = new java.io.FileWriter(file);
            myWriter.write(text);
            myWriter.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}
