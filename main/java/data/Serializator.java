package data;

import businessLogic.MenuItem;
import businessLogic.Order;
import businessLogic.User;

import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class Serializator {
    public static void serializeProducts(Object o) {
        serialize(o, "src\\main\\resources\\AllProducts.txt");
    }

    public static void serializeOrders(Object o) {
        serialize(o, "src\\main\\resources\\AllOrders.txt");
    }

    public static void serializeUsers(Object o) {
        serialize(o, "src\\main\\resources\\AllUsers.txt");
    }

    private static void serialize(Object o, String filePath) {
        try {
            FileOutputStream file = new FileOutputStream(filePath);
            ObjectOutputStream out = new ObjectOutputStream(file);
            out.writeObject(o); // Method for serialization of object
            out.close();
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Set<MenuItem> deserializeProducts() {
        Set<MenuItem> allProducts = new HashSet<>();
        try {
            FileInputStream file = new FileInputStream("src\\main\\resources\\AllProducts.txt");
            ObjectInputStream in = new ObjectInputStream(file);
            allProducts = (Set<MenuItem>) in.readObject(); // Method for deserialization of object
            in.close();
            file.close();
        } catch (IOException | ClassNotFoundException e) {
            //e.printStackTrace();
        }
        return allProducts;
    }

    public static HashMap<Order, List<MenuItem>> deserializeOrders() {
        HashMap<Order, List<MenuItem>> allOrders = null;
        try {
            FileInputStream file = new FileInputStream("src\\main\\resources\\AllOrders.txt");
            ObjectInputStream in = new ObjectInputStream(file);
            allOrders = (HashMap<Order, List<MenuItem>>) in.readObject(); // Method for deserialization of object
            in.close();
            file.close();
        } catch (IOException | ClassNotFoundException e) {
            //e.printStackTrace();
        }
        return allOrders;
    }

    public static Set<User> deserializeUsers() {
        Set<User> allUsers = null;
        try {
            FileInputStream file = new FileInputStream("src\\main\\resources\\AllUsers.txt");
            ObjectInputStream in = new ObjectInputStream(file);
            allUsers = (Set<User>) in.readObject(); // Method for deserialization of object
            in.close();
            file.close();
        } catch (IOException | ClassNotFoundException e) {
            //e.printStackTrace();
        }
        return allUsers;
    }
}
