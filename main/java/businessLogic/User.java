package businessLogic;

import java.io.Serializable;
import java.util.Objects;

public class User implements Serializable {
    private final String username;
    private final String password;
    private final UserType userType;
    private final String email;
    private static int noUsers;
    private final int id;

    public User(String username, String password, UserType userType, String email) {
        this.username = username;
        this.password = password;
        this.userType = userType;
        this.email = email;
        noUsers++;
        id = noUsers;
    }

    public static void setNoUsers(int noUsers) {
        User.noUsers = noUsers;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public UserType getUserType() {
        return userType;
    }

    public static int getNoUsers() {
        return noUsers;
    }

    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public String toString() {
        return "User{" +
                "username='" + username + '\'' +
                ", userType=" + userType +
                ", email='" + email + '\'' +
                ", id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(username, user.username) &&
                Objects.equals(email, user.email);
    }

}
