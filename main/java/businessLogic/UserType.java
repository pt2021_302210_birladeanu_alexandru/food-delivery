package businessLogic;

public enum UserType {
    ADMINISTRATOR,
    CLIENT,
    EMPLOYEE
}
