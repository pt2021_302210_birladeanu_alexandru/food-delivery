package businessLogic;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;

public class Order implements Serializable {
    private static int noOrders;
    private final int orderId;
    private final int clientId;
    private final LocalDateTime date;
    private int price;
    private boolean ready;

    public Order(User user) {
        noOrders++;
        orderId = noOrders;
        this.clientId = user.getId();
        date = LocalDateTime.now();
        ready = false;
    }

    public static void setNoOrders(int noOrders) {
        Order.noOrders = noOrders;
    }

    public static int getNoOrders() {
        return noOrders;
    }

    public int getOrderId() {
        return orderId;
    }

    public int getClientId() {
        return clientId;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", clientId=" + clientId +
                ", date=" + date +
                ", price=" + price +
                '}';
    }

    public boolean isReady() {
        return ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId);
    }
}
