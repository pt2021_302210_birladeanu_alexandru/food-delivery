package businessLogic;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvException;
import data.FileWriter;
import data.Serializator;

import java.io.*;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class DeliveryService extends Observable implements IDeliveryServiceProcessing {

    public DeliveryService() {
        if (Serializator.deserializeOrders() != null)
            Order.setNoOrders(Serializator.deserializeOrders().size());
        if (Serializator.deserializeUsers() != null)
            User.setNoUsers(Serializator.deserializeUsers().size());
        else {
            Set<User> users = new HashSet<>();
            users.add(new User("admin", "admin", UserType.ADMINISTRATOR, "ionutLenghel@gmail.com"));
            Serializator.serializeUsers(users);
        }
    }

    /**
     * @pre file must not be empty
     * @post serialize all MenuItems from products.csv to AllProducts.txt
     */
    @Override
    public void importInitialProducts() {
        String filePath = "src\\main\\resources\\products.csv";
        Set<MenuItem> allProducts = Serializator.deserializeProducts();
        try (CSVReader reader = new CSVReader(new FileReader(filePath))) {
            List<String[]> r = reader.readAll();
            r.remove(0);//the column headers
            r.forEach(x -> allProducts.add(new BaseProduct(Arrays.toString(x))));
        } catch (CsvException | IOException e) {
            e.printStackTrace();
        }
        assert isWellFormed();
        Serializator.serializeProducts(allProducts);
    }

    /**
     * @param menuItem the new item which will be added to the set
     * @pre menuItem must not be null
     * @post menuItem will be added to the serialized set
     */
    @Override
    public void addProduct(MenuItem menuItem) {
        assert menuItem!=null;
        Set<MenuItem> allProducts = Serializator.deserializeProducts();
        allProducts.add(menuItem);
        Serializator.serializeProducts(allProducts);
        assert allProducts.contains(menuItem);
        assert isWellFormed();
    }

    /**
     * @param menuItem the item which will be deleted
     * @pre menuItem must exist in the serialized set
     * @post menuItem will be deleted from the serialized set
     */
    @Override
    public void deleteProduct(MenuItem menuItem) {
        Set<MenuItem> allProducts = Serializator.deserializeProducts();
        assert allProducts.contains(menuItem);
        allProducts.remove(menuItem);
        Serializator.serializeProducts(allProducts);
        assert !allProducts.contains(menuItem);
        assert isWellFormed();
    }

    /**
     * @param startHour
     * @param endHour
     * @return the text which will also be written in the txt file
     * @pre valid hours
     * @post a report will be generated with all the orders performed in the given interval
     */
    @Override
    public String generateReport1(int startHour, int endHour) {
        assert (startHour>=0 && endHour>=0);
        HashMap<Order, List<MenuItem>> orderedProducts = Serializator.deserializeOrders();
        StringBuilder output = new StringBuilder("Orders made between the hours " + startHour + " and " + endHour + ":" + '\n' + '\n');
        Set<Order> orderSet = orderedProducts.keySet().stream()
                .filter(order -> order.getDate().getHour() >= startHour && order.getDate().getHour() <= endHour)
                .collect(Collectors.toCollection(HashSet::new));
        orderSet.stream().forEach(order -> output.append("\t").append(order.toString()).append("\n"));

        FileWriter.writeReport(output.toString());
        assert output.toString().contains("Orders made between the hours " + startHour + " and " + endHour + ":" + '\n' + '\n');
        assert isWellFormed();
        return output.toString();
    }

    /**
     * @param timesOrdered no of times a product has been ordered
     * @return the text which will also be written in the txt file
     * @pre timesOrdered must be greater than 0
     * @post a report will be generated with all the products ordered at least timesOrdered times
     */
    @Override
    public String generateReport2(int timesOrdered) {
        assert timesOrdered>0;
        HashMap<Order, List<MenuItem>> orderedProducts = Serializator.deserializeOrders();
        StringBuilder output = new StringBuilder("Products ordered multiple times:" + '\n' + '\n');

        List<MenuItem> products = new LinkedList<>();
        orderedProducts.keySet().stream().forEach(order -> products.addAll(orderedProducts.get(order)));
        HashMap<MenuItem, Long> productFrequency = products.stream()
                .collect(Collectors.groupingBy(
                        Function.identity(),
                        HashMap::new,
                        Collectors.counting()
                ));
        Set<MenuItem> validProducts = productFrequency.keySet().stream().filter(menuItem -> productFrequency.get(menuItem) >= timesOrdered).collect(Collectors.toCollection(HashSet::new));
        validProducts.stream().forEach(menuItem -> output.append(menuItem.toString()).append("\n\t\tWas Ordered ").append(productFrequency.get(menuItem)).append(" times."));

        FileWriter.writeReport(output.toString());
        assert output.toString().contains("Products ordered multiple times:" + '\n' + '\n');
        assert isWellFormed();
        return output.toString();
    }

    /**
     * @param timesOrdered
     * @param lowPriceBound
     * @return the text which will also be written in the txt file
     * @pre the parameters must be values greater than 0
     * @post a report will be generated with all the clients who placed at least timesOrdered orders more expensive than lowPriceBound
     */
    @Override
    public String generateReport3(int timesOrdered, int lowPriceBound) {
        assert (timesOrdered>0 && lowPriceBound>=0);
        Set<Order> orders = Serializator.deserializeOrders().keySet().stream().filter(order -> order.getPrice() >= lowPriceBound).collect(Collectors.toCollection(HashSet::new));
        StringBuilder output = new StringBuilder("Clients who have ordered multiple times, all of their orders being more expensive than " + lowPriceBound + ":" + '\n' + '\n');
        List<Integer> usersIds = new LinkedList<>();
        orders.stream().forEach(order -> usersIds.add(order.getClientId()));
        HashMap<Integer, Long> userIdFrequency = usersIds.stream().collect(Collectors.groupingBy(Function.identity(), HashMap::new, Collectors.counting()));
        Set<Integer> validUserIds = userIdFrequency.keySet().stream().filter(integer -> userIdFrequency.get(integer) >= timesOrdered).collect(Collectors.toCollection(HashSet::new));
        for (User u : Serializator.deserializeUsers()) {
            if (validUserIds.contains(u.getId()))
                output.append("\n").append(u.toString());
        }
        FileWriter.writeReport(output.toString());
        assert output.toString().contains("Clients who have ordered multiple times, all of their orders being more expensive than " + lowPriceBound + ":" + '\n' + '\n');
        assert isWellFormed();
        return output.toString();
    }

    /**
     * @param day
     * @return the text which will also be written in the txt file
     * @pre day must be between 1 and 31
     * @post a report will be generated with all the products ordered on this day of the month
     */
    @Override
    public String generateReport4(int day) {
        assert (day>0 && day<=31);
        StringBuilder output = new StringBuilder(switch (day) {
            case 1 -> "Products ordered on the " + day + "st day of the month:" + '\n';
            case 2 -> "Products ordered on the " + day + "nd day of the month:" + '\n';
            case 3 -> "Products ordered on the " + day + "rd day of the month:" + '\n';
            default -> "Products ordered on the " + day + "th day of the month:" + '\n';
        });
        HashMap<Order, List<MenuItem>> ordersMap = Serializator.deserializeOrders();
        Set<Order> validOrders = ordersMap.keySet().stream().filter(order -> order.getDate().getDayOfMonth() == day).collect(Collectors.toCollection(HashSet::new));
        List<MenuItem> products = new LinkedList<>();
        validOrders.stream().forEach(order -> products.addAll(ordersMap.get(order)));
        HashMap<MenuItem, Long> productFrequency = products.stream()
                .collect(Collectors.groupingBy(
                        Function.identity(),
                        HashMap::new,
                        Collectors.counting()
                ));
        productFrequency.keySet().stream().forEach(menuItem -> output.append("\n").append(menuItem.toString()).append("\n\t").append("Was ordered ").append(productFrequency.get(menuItem)).append(" times."));
        FileWriter.writeReport(output.toString());
        assert output.toString().contains("Products ordered on the ");
        assert isWellFormed();
        return output.toString();
    }

    /**
     * @return a set with the menu items from AllProducts.txt
     * @pre the file from which the set is deserialized must not be null
     * @post the menu items will be deserialized
     */
    @Override
    public Set<MenuItem> viewProducts() {
        assert isWellFormed();
        return Serializator.deserializeProducts();
    }

    /**
     * @param productName
     * @return a set with all the products whose titles contain the parameter productName
     * @pre there must be products whose titles contain the parameter
     * @post all the products whose titles contain the parameter productName are found
     */
    @Override
    public Set<MenuItem> searchProductByName(String productName) {
        Set<MenuItem> filtered = Serializator.deserializeProducts();
        filtered = filtered.stream().filter(i -> i.getTitle().toLowerCase().contains(productName.toLowerCase())).collect(Collectors.toCollection(HashSet::new));
        assert isWellFormed();
        return filtered;
    }

    /**
     * @param products  items from which to filter
     * @param minRating
     * @param maxRating
     * @return filtered items
     * @pre give a valid interval and set in which to search
     * @post select only the items whose ratings are in the given interval
     */
    @Override
    public Set<MenuItem> searchProductByRating(Set<MenuItem> products, float minRating, float maxRating) {
        assert (minRating>0 && maxRating>0 && maxRating>=minRating);
        products = products.stream().filter(i -> i.getRating() >= minRating && i.getRating() <= maxRating).collect(Collectors.toCollection(HashSet::new));
        assert isWellFormed();
        return products;
    }

    /**
     * @param products     items from which to filter
     * @param lowCalories
     * @param highCalories
     * @return filtered items
     * @pre give a valid interval and set in which to search
     * @post select only the items whose calories are in the given interval
     */
    @Override
    public Set<MenuItem> searchProductByCalories(Set<MenuItem> products, int lowCalories, int highCalories) {
        assert (lowCalories>0 && highCalories>0 && highCalories>=lowCalories);
        products = products.stream().filter(i -> i.getCalories() >= lowCalories && i.getCalories() <= highCalories).collect(Collectors.toCollection(HashSet::new));
        assert isWellFormed();
        return products;
    }

    /**
     * @param products    items from which to filter
     * @param lowProtein
     * @param highProtein
     * @return filtered items
     * @pre give a valid interval and set in which to search
     * @post select only the items whose proteins are in the given interval
     */
    @Override
    public Set<MenuItem> searchProductByProtein(Set<MenuItem> products, int lowProtein, int highProtein) {
        assert (lowProtein>0 && highProtein>0 && highProtein>=lowProtein);
        products = products.stream().filter(i -> i.getProtein() >= lowProtein && i.getProtein() <= highProtein).collect(Collectors.toCollection(HashSet::new));
        assert isWellFormed();
        return products;
    }

    /**
     * @param products items from which to filter
     * @param lowFat
     * @param highFat
     * @return filtered items
     * @pre give a valid interval and set in which to search
     * @post select only the items whose fats are in the given interval
     */
    @Override
    public Set<MenuItem> searchProductByFat(Set<MenuItem> products, int lowFat, int highFat) {
        assert (lowFat>0 && highFat>0 && highFat>=lowFat);
        products = products.stream().filter(i -> i.getFat() >= lowFat && i.getFat() <= highFat).collect(Collectors.toCollection(HashSet::new));
        assert isWellFormed();
        return products;
    }

    /**
     * @param products   items from which to filter
     * @param lowSodium
     * @param highSodium
     * @return filtered items
     * @pre give a valid interval and set in which to search
     * @post select only the items whose sodium quantity is in the given interval
     */
    @Override
    public Set<MenuItem> searchProductBySodium(Set<MenuItem> products, int lowSodium, int highSodium) {
        assert (lowSodium>0 && highSodium>0 && highSodium>=lowSodium);
        products = products.stream().filter(i -> i.getSodium() >= lowSodium && i.getSodium() <= highSodium).collect(Collectors.toCollection(HashSet::new));
        assert isWellFormed();
        return products;
    }

    /**
     * @param products  items from which to filter
     * @param lowPrice
     * @param highPrice
     * @return filtered items
     * @pre give a valid interval and set in which to search
     * @post select only the items whose price is in the given interval
     */
    @Override
    public Set<MenuItem> searchProductByPrice(Set<MenuItem> products, int lowPrice, int highPrice) {
        assert (lowPrice>0 && highPrice>0 && highPrice>=lowPrice);
        products = products.stream().filter(i -> i.getPrice() >= lowPrice && i.getPrice() <= highPrice).collect(Collectors.toCollection(HashSet::new));
        assert isWellFormed();
        return products;
    }

    /**
     * @param user the user who places the order
     * @param menu the items ordered
     * @return the newly created order
     * @pre give an existing user and valid items
     * @postcreate a new order, serialize it, notify the observers(the employee views) and create a bill
     */
    @Override
    public Order createOrder(User user, List<MenuItem> menu) {
        assert menu!=null;
        Order order = new Order(user);
        int orderPrice = 0;
        for (MenuItem i : menu) {
            orderPrice += i.getPrice();
        }
        order.setPrice(orderPrice);
        HashMap<Order, List<MenuItem>> orders = Serializator.deserializeOrders();
        if (orders == null)
            orders = new HashMap<>();
        orders.put(order, menu);
        Serializator.serializeOrders(orders);
        FileWriter.writeBill(order.toString() + '\n' + menu);

        setChanged();
        notifyObservers();
        assert isWellFormed();
        return order;
    }

    private boolean isWellFormed(){
        return  true;
    }
}
