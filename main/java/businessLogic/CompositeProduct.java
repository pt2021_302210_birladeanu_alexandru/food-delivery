package businessLogic;

import java.util.List;

public class CompositeProduct extends MenuItem {
    List<MenuItem> composition;

    public CompositeProduct(String name, float rating, List<MenuItem> composition) {
        this.composition = composition;
        super.setTitle(name);
        for (MenuItem b : composition) {
            super.setRating(rating);
            super.setCalories(super.getCalories() + b.getCalories());
            super.setProtein(super.getProtein() + b.getProtein());
            super.setFat(super.getFat() + b.getFat());
            super.setSodium(super.getSodium() + b.getSodium());
            super.setPrice(super.getPrice() + b.computePrice());
        }
    }


    @Override
    public int computePrice() {
        return super.getPrice();
    }
}
