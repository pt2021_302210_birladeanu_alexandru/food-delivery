package businessLogic;

public class BaseProduct extends MenuItem {

    public BaseProduct(String title, float rating, int calories, int protein, int fat, int sodium, int price) {
        super.setTitle(title);
        this.setRating(rating);
        this.setCalories(calories);
        this.setProtein(protein);
        this.setFat(fat);
        this.setSodium(sodium);
        this.setPrice(price);
    }

    public BaseProduct(String csvLine) {
        csvLine = csvLine.substring(1, csvLine.length() - 1);//remove '[' and ']' from the line
        String[] res = csvLine.split(",", 0);
        this.setTitle(res[0].stripTrailing().stripLeading());
        this.setRating(Float.parseFloat(res[1].stripTrailing().stripLeading()));
        this.setCalories(Integer.parseInt(res[2].stripTrailing().stripLeading()));
        this.setProtein(Integer.parseInt(res[3].stripTrailing().stripLeading()));
        this.setFat(Integer.parseInt(res[4].stripTrailing().stripLeading()));
        this.setSodium(Integer.parseInt(res[5].stripTrailing().stripLeading()));
        this.setPrice(Integer.parseInt(res[6].stripTrailing().stripLeading()));
    }

    @Override
    public int computePrice() {
        return super.getPrice();
    }
}
