package businessLogic;

import java.util.List;
import java.util.Set;

public interface IDeliveryServiceProcessing {
    /**
     * @pre file must not be empty
     * @post serialize all MenuItems from products.csv to AllProducts.txt
     */
    void importInitialProducts();

    /**
     * @param menuItem the new item which will be added to the set
     * @pre menuItem must not be null
     * @post menuItem will be added to the serialized set
     */
    void addProduct(MenuItem menuItem);

    /**
     * @param menuItem the item which will be deleted
     * @pre menuItem must exist in the serialized set
     * @post menuItem will be deleted from the serialized set
     */
    void deleteProduct(MenuItem menuItem);

    /**
     * @param startHour
     * @param endHour
     * @return the text which will also be written in the txt file
     * @pre valid hours
     * @post a report will be generated with all the orders performed in the given interval
     */
    String generateReport1(int startHour, int endHour);

    /**
     * @param timesOrdered no of times a product has been ordered
     * @return the text which will also be written in the txt file
     * @pre timesOrdered must be greater than 0
     * @post a report will be generated with all the products ordered at least timesOrdered times
     */
    String generateReport2(int timesOrdered);

    /**
     * @param timesOrdered
     * @param lowPriceBound
     * @return the text which will also be written in the txt file
     * @pre the parameters must be values greater than 0
     * @post a report will be generated with all the clients who placed at least timesOrdered orders more expensive than lowPriceBound
     */
    String generateReport3(int timesOrdered, int lowPriceBound);

    /**
     * @param day
     * @return the text which will also be written in the txt file
     * @pre day must be between 1 and 31
     * @post a report will be generated with all the products ordered on this day of the month
     */
    String generateReport4(int day);

    /**
     * @return a set with the menu items from AllProducts.txt
     * @pre the file from which the set is deserialized must not be null
     * @post the menu items will be deserialized
     */
    Set<MenuItem> viewProducts();

    /**
     * @param productName
     * @return a set with all the products whose titles contain the parameter productName
     * @pre there must be products whose titles contain the parameter
     * @post all the products whose titles contain the parameter productName are found
     */
    Set<MenuItem> searchProductByName(String productName);

    /**
     * @param products  items from which to filter
     * @param minRating
     * @param maxRating
     * @return filtered items
     * @pre give a valid interval and set in which to search
     * @post select only the items whose ratings are in the given interval
     */
    Set<MenuItem> searchProductByRating(Set<MenuItem> products, float minRating, float maxRating);

    /**
     * @param products     items from which to filter
     * @param lowCalories
     * @param highCalories
     * @return filtered items
     * @pre give a valid interval and set in which to search
     * @post select only the items whose calories are in the given interval
     */
    Set<MenuItem> searchProductByCalories(Set<MenuItem> products, int lowCalories, int highCalories);

    /**
     * @param products    items from which to filter
     * @param lowProtein
     * @param highProtein
     * @return filtered items
     * @pre give a valid interval and set in which to search
     * @post select only the items whose proteins are in the given interval
     */
    Set<MenuItem> searchProductByProtein(Set<MenuItem> products, int lowProtein, int highProtein);

    /**
     * @param products items from which to filter
     * @param lowFat
     * @param highFat
     * @return filtered items
     * @pre give a valid interval and set in which to search
     * @post select only the items whose fats are in the given interval
     */
    Set<MenuItem> searchProductByFat(Set<MenuItem> products, int lowFat, int highFat);

    /**
     * @param products   items from which to filter
     * @param lowSodium
     * @param highSodium
     * @return filtered items
     * @pre give a valid interval and set in which to search
     * @post select only the items whose sodium quantity is in the given interval
     */
    Set<MenuItem> searchProductBySodium(Set<MenuItem> products, int lowSodium, int highSodium);

    /**
     * @param products  items from which to filter
     * @param lowPrice
     * @param highPrice
     * @return filtered items
     * @pre give a valid interval and set in which to search
     * @post select only the items whose price is in the given interval
     */
    Set<MenuItem> searchProductByPrice(Set<MenuItem> products, int lowPrice, int highPrice);

    /**
     * @param user the user who places the order
     * @param menu the items ordered
     * @return the newly created order
     * @pre give an existing user and valid items
     * @postcreate a new order, serialize it, notify the observers(the employee views) and create a bill
     */
    Order createOrder(User user, List<MenuItem> menu);
}
