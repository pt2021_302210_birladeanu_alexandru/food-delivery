package presentation;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

public class SignUpView extends JFrame {
    private final JTextField textFieldEmail;
    private final JTextField textFieldUsername;
    private final JTextField textFieldPassword;
    private final JTextField textFieldConfirmPassword;
    private final JButton doneButton;

    public SignUpView() {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(0, 0, 800, 599);
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel lblNewLabel = new JLabel("Create Account");
        lblNewLabel.setFont(new Font("Tempus Sans ITC", Font.BOLD, 45));
        lblNewLabel.setForeground(Color.BLACK);
        lblNewLabel.setBounds(52, 0, 314, 60);
        contentPane.add(lblNewLabel);

        JLabel lblNewLabel_1 = new JLabel("Email");
        lblNewLabel_1.setForeground(Color.BLACK);
        lblNewLabel_1.setFont(new Font("Tempus Sans ITC", Font.BOLD, 40));
        lblNewLabel_1.setBounds(52, 90, 169, 76);
        contentPane.add(lblNewLabel_1);

        textFieldEmail = new JTextField();
        textFieldEmail.setFont(new Font("Tahoma", Font.PLAIN, 20));
        textFieldEmail.setBounds(392, 115, 168, 31);
        contentPane.add(textFieldEmail);
        textFieldEmail.setColumns(10);

        JLabel lblNewLabel_2 = new JLabel("Username");
        lblNewLabel_2.setFont(new Font("Tempus Sans ITC", Font.BOLD, 40));
        lblNewLabel_2.setForeground(Color.BLACK);
        lblNewLabel_2.setBounds(52, 221, 180, 76);
        contentPane.add(lblNewLabel_2);

        textFieldUsername = new JTextField();
        textFieldUsername.setFont(new Font("Tahoma", Font.PLAIN, 20));
        textFieldUsername.setBounds(392, 250, 168, 31);
        contentPane.add(textFieldUsername);
        textFieldUsername.setColumns(10);

        JLabel lblNewLabel_3 = new JLabel("Password");
        lblNewLabel_3.setFont(new Font("Tempus Sans ITC", Font.BOLD, 40));
        lblNewLabel_3.setForeground(Color.BLACK);
        lblNewLabel_3.setBounds(52, 361, 180, 76);
        contentPane.add(lblNewLabel_3);

        textFieldPassword = new JPasswordField();
        textFieldPassword.setFont(new Font("Tahoma", Font.PLAIN, 20));
        textFieldPassword.setBounds(392, 387, 168, 31);
        contentPane.add(textFieldPassword);
        textFieldPassword.setColumns(10);

        JLabel lblNewLabel_4 = new JLabel("Confirm Password");
        lblNewLabel_4.setFont(new Font("Tempus Sans ITC", Font.BOLD, 40));
        lblNewLabel_4.setForeground(Color.BLACK);
        lblNewLabel_4.setBounds(52, 470, 330, 76);
        contentPane.add(lblNewLabel_4);

        textFieldConfirmPassword = new JPasswordField();
        textFieldConfirmPassword.setFont(new Font("Tahoma", Font.PLAIN, 20));
        textFieldConfirmPassword.setBounds(392, 495, 168, 31);
        contentPane.add(textFieldConfirmPassword);
        textFieldConfirmPassword.setColumns(10);

        doneButton = new JButton("Done");
        doneButton.setFont(new Font("Tempus Sans ITC", Font.BOLD, 20));
        doneButton.setBackground(new Color(212, 4, 36));
        doneButton.setForeground(Color.white);
        doneButton.setBounds(670, 530, 110, 30);
        contentPane.add(doneButton);

        try {
            JLabel photoLabel = new JLabel(new ImageIcon(ImageIO.read(new File("src\\main\\resources\\SignUpBackgroundLarge.jpg"))));
            photoLabel.setBounds(0, 0, 800, 599);
            contentPane.add(photoLabel);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.setVisible(true);
    }

    public String getEmail() {
        return textFieldEmail.getText();
    }

    public String getUsername() {
        return textFieldUsername.getText();
    }

    public String getPassword() {
        return textFieldPassword.getText();
    }

    public String getConfirmedPassword() {
        return textFieldConfirmPassword.getText();
    }

    public void doneButtonListener(ActionListener actionListener) {
        this.doneButton.addActionListener(actionListener);
    }


}
