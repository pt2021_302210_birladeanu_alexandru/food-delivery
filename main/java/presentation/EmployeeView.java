package presentation;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

public class EmployeeView extends JFrame implements Observer {
    private final JPanel contentPane;
    private JScrollPane scrollPane;
    private final JButton btnView;
    private final JButton btnContents;
    private final JButton btnReady;

    public EmployeeView() {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(0, 0, 800, 449);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        btnView = new JButton("View Orders");
        btnView.setFont(new Font("Tempus Sans ITC", Font.BOLD, 20));
        btnView.setForeground(Color.WHITE);
        btnView.setBackground(new Color(97, 131, 158));
        btnView.setBounds(50, 40, 205, 39);
        contentPane.add(btnView);

        btnContents = new JButton("Read Order");
        btnContents.setFont(new Font("Tempus Sans ITC", Font.BOLD, 20));
        btnContents.setForeground(Color.WHITE);
        btnContents.setBackground(new Color(97, 131, 158));
        btnContents.setBounds(50, 90, 205, 39);
        contentPane.add(btnContents);

        btnReady = new JButton("Mark as Ready");
        btnReady.setFont(new Font("Tempus Sans ITC", Font.BOLD, 20));
        btnReady.setForeground(Color.WHITE);
        btnReady.setBackground(new Color(97, 131, 158));
        btnReady.setBounds(50, 140, 205, 39);
        contentPane.add(btnReady);

        try {
            JLabel photoLabel = new JLabel(new ImageIcon(ImageIO.read(new File("src\\main\\resources\\EmployeeBackground.jpg"))));
            photoLabel.setBounds(0, 0, 800, 449);
            contentPane.add(photoLabel);
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.setVisible(true);
    }

    public void viewButtonListener(ActionListener actionListener) {
        this.btnView.addActionListener(actionListener);
    }

    public void contentsButtonListener(ActionListener actionListener) {
        this.btnContents.addActionListener(actionListener);
    }

    public void readyButtonListener(ActionListener actionListener) {
        this.btnReady.addActionListener(actionListener);
    }

    public void addTable(JScrollPane scrollPane) {
        if (this.scrollPane != null)
            contentPane.remove(this.scrollPane);
        this.scrollPane = scrollPane;
        scrollPane.setBounds(348, 10, 428, 392);
        contentPane.add(scrollPane);
    }

    @Override
    public void update(Observable o, Object arg) {
        System.out.println("???????");
        JOptionPane.showMessageDialog(this, "A new order has been placed! Please refresh the orders' table!");
    }
}
