package presentation;

import businessLogic.*;
import data.Serializator;

import javax.swing.*;
import java.util.*;

public class Controller {
    private JTable jTable;
    private Object[][] data;
    private HashMap<Order, List<MenuItem>> unfinishedOrders;
    private final LoginView loginView;
    DeliveryService deliveryService;

    public Controller() {
        deliveryService = new DeliveryService();
        loginView = new LoginView();
        loginView.signupButtonListener(e -> {
            SignUpView signUpView = new SignUpView();
            signUpView.doneButtonListener(e1 -> {
                createUser(signUpView, UserType.CLIENT);
                signUpView.dispose();
            });
        });
        loginView.loginButtonListener(e -> {
            User currentUser = null;
            Set<User> allUsers = Serializator.deserializeUsers();
            for (User u : allUsers) {
                if (u.getUsername().equals(loginView.getUsername())) {
                    currentUser = u;
                    break;
                }
            }
            if (currentUser == null) {
                JOptionPane.showMessageDialog(loginView, "Username does not exist!");
            } else {
                if (!currentUser.getPassword().equals(loginView.getPassword())) {
                    JOptionPane.showMessageDialog(loginView, "Wrong password!");
                } else {
                    switch (currentUser.getUserType()) {
                        case ADMINISTRATOR -> openAdministratorPage();
                        case CLIENT -> openClientPage(currentUser);
                        case EMPLOYEE -> openEmployeePage();
                    }
                }
            }
        });
    }

    private void openAdministratorPage() {
        AdminView adminView = new AdminView();
        adminView.viewProductsButtonListener(e -> showAdminTable(Serializator.deserializeProducts(), adminView));
        adminView.addStaffAccountButtonListener(e -> {
            SignUpView signUpView = new SignUpView();
            signUpView.doneButtonListener(e1 -> {
                createUser(signUpView, UserType.EMPLOYEE);
                signUpView.dispose();
            });
        });
        adminView.importProductsButtonListener(e -> deliveryService.importInitialProducts());

        adminView.deleteProductButtonListener(e -> {
            if (jTable != null) {
                Set<MenuItem> found = deliveryService.searchProductByName(jTable.getValueAt(jTable.getSelectedRow(), 0).toString());
                for (MenuItem m : found) {// found will always have just one element
                    int result = JOptionPane.showConfirmDialog(adminView, "Are you sure you want to delete\n" + m.toString() + " ?");
                    if (result == JOptionPane.YES_OPTION) {
                        deliveryService.deleteProduct(m);
                        showAdminTable(deliveryService.viewProducts(), adminView);
                    }
                }
            }
        });
        adminView.searchByNameButtonListener(e -> showAdminTable(deliveryService.searchProductByName(adminView.getTextFieldSearch()), adminView));
        adminView.generateReportButtonListener(e -> new ReportView());
        adminView.saveChangesButtonListener(e -> {
            Set<MenuItem> allProducts = new HashSet<>();
            for (int i = 0; i < jTable.getRowCount(); i++) {
                allProducts.add(new BaseProduct(data[i][0].toString(), Float.parseFloat(data[i][1].toString()), Integer.parseInt(data[i][2].toString()), Integer.parseInt(data[i][3].toString()), Integer.parseInt(data[i][4].toString()), Integer.parseInt(data[i][5].toString()), Integer.parseInt(data[i][6].toString())));
            }
            Serializator.serializeProducts(allProducts);
        });
        adminView.addProductButtonListener(e -> addProduct());
    }

    private void openClientPage(User currentClient) {
        ClientView clientView = new ClientView();
        clientView.viewProductsButtonListener(e -> {
            try {
                Set<MenuItem> filtered = deliveryService.searchProductByName(clientView.getTitle());
                float minRating, maxRating;
                if (clientView.getMinRating().equals(""))
                    minRating = 0;
                else
                    minRating = Float.parseFloat(clientView.getMinRating());
                if (clientView.getMaxRating().equals(""))
                    maxRating = Float.MAX_VALUE;
                else
                    maxRating = Float.parseFloat(clientView.getMaxRating());
                filtered = deliveryService.searchProductByRating(filtered, minRating, maxRating);
                filtered = deliveryService.searchProductByCalories(filtered, getMinValue(clientView.getMinCalories()), getMaxValue(clientView.getMaxCalories()));
                filtered = deliveryService.searchProductByProtein(filtered, getMinValue(clientView.getMinProtein()), getMaxValue(clientView.getMaxProtein()));
                filtered = deliveryService.searchProductByFat(filtered, getMinValue(clientView.getMinFat()), getMaxValue(clientView.getMaxFat()));
                filtered = deliveryService.searchProductBySodium(filtered, getMinValue(clientView.getMinSodium()), getMaxValue(clientView.getMaxSodium()));
                filtered = deliveryService.searchProductByPrice(filtered, getMinValue(clientView.getMinPrice()), getMaxValue(clientView.getMaxPrice()));
                showClientTable(filtered, clientView);
            } catch (NumberFormatException nfe) {
                JOptionPane.showMessageDialog(null, "Bad Input!");
            }
        });
        List<MenuItem> composition = new LinkedList<>();
        clientView.addToCartButtonListener(e -> {
            if (jTable != null) {
                int[] selected = jTable.getSelectedRows();

                for (int j : selected) {
                    composition.add(new BaseProduct(jTable.getValueAt(j, 0).toString(),
                            Float.parseFloat(jTable.getValueAt(j, 1).toString()),
                            Integer.parseInt(jTable.getValueAt(j, 2).toString()),
                            Integer.parseInt(jTable.getValueAt(j, 3).toString()),
                            Integer.parseInt(jTable.getValueAt(j, 4).toString()),
                            Integer.parseInt(jTable.getValueAt(j, 5).toString()),
                            Integer.parseInt(jTable.getValueAt(j, 6).toString())));
                }
                JOptionPane.showMessageDialog(null, "Cart Contents:\n" + composition);
            }
        });
        clientView.emptyCartButtonListener(e -> {
            composition.clear();
            JOptionPane.showMessageDialog(null, "Your cart is now empty!");
        });
        clientView.orderButtonListener(e -> {
            if (composition.size() > 0) {
                Order newOrder = deliveryService.createOrder(currentClient, composition);
                JOptionPane.showMessageDialog(null, "Your order has been sent!\n" + newOrder.toString() + "\ncontains\n" + composition);
                composition.clear();
            } else
                JOptionPane.showMessageDialog(null, "Your cart is currently empty!");
        });

    }

    private void openEmployeePage() {
        EmployeeView employeeView = new EmployeeView();
        deliveryService.addObserver(employeeView);
        employeeView.viewButtonListener(e -> showEmployeeTable(employeeView));
        employeeView.contentsButtonListener(e -> {
            if (jTable != null && jTable.getSelectedRow() != -1) {
                Order selectedOrder = findOrderById(Integer.parseInt(jTable.getValueAt(jTable.getSelectedRow(), 0).toString()));
                if (selectedOrder != null) {
                    JOptionPane.showMessageDialog(null, "Order " + selectedOrder.getOrderId() + "\nContents:\n" + unfinishedOrders.get(selectedOrder));
                }
            } else {
                JOptionPane.showMessageDialog(null, "No order has been selected!");
            }
        });
        employeeView.readyButtonListener(e -> {
            if (jTable != null && jTable.getSelectedRow() != -1) {
                HashMap<Order, List<MenuItem>> allOrders = Serializator.deserializeOrders();
                for (Order o : allOrders.keySet()) {
                    if (o.getOrderId() == Integer.parseInt(jTable.getValueAt(jTable.getSelectedRow(), 0).toString()))
                        o.setReady(true);
                }
                Serializator.serializeOrders(allOrders);
                showEmployeeTable(employeeView);
            } else {
                JOptionPane.showMessageDialog(null, "No order has been selected!");
            }
        });
    }

    private void showAdminTable(Set<MenuItem> products, AdminView adminView) {
        String[] columns = {"Title", "Rating", "Calories", "Protein", "Fat", "Sodium", "Price"};
        data = new Object[products.size()][7];
        int row = 0;
        for (MenuItem i : products) {
            data[row][0] = i.getTitle();
            data[row][1] = i.getRating();
            data[row][2] = i.getCalories();
            data[row][3] = i.getProtein();
            data[row][4] = i.getFat();
            data[row][5] = i.getSodium();
            data[row][6] = i.getPrice();
            row++;
        }
        jTable = new JTable(data, columns);
        JScrollPane jsp = new JScrollPane(jTable);
        adminView.addTable(jsp);
    }

    private void showClientTable(Set<MenuItem> products, ClientView clientView) {
        String[] columns = {"Title", "Rating", "Calories", "Protein", "Fat", "Sodium", "Price"};
        data = new Object[products.size()][7];
        int row = 0;
        for (MenuItem i : products) {
            data[row][0] = i.getTitle();
            data[row][1] = i.getRating();
            data[row][2] = i.getCalories();
            data[row][3] = i.getProtein();
            data[row][4] = i.getFat();
            data[row][5] = i.getSodium();
            data[row][6] = i.getPrice();
            row++;
        }
        jTable = new JTable(data, columns);
        JScrollPane jsp = new JScrollPane(jTable);
        clientView.addTable(jsp);
    }

    private void showEmployeeTable(EmployeeView employeeView) {
        unfinishedOrders = new HashMap<>();
        HashMap<Order, List<MenuItem>> allOrders = Serializator.deserializeOrders();
        for (Order o : allOrders.keySet()) {
            if (!o.isReady())
                unfinishedOrders.put(o, allOrders.get(o));
        }

        String[] columns = {"Order ID", "Client ID", "Date", "Price"};
        data = new Object[unfinishedOrders.keySet().size()][4];
        int row = 0;
        for (Order o : unfinishedOrders.keySet()) {
            data[row][0] = o.getOrderId();
            data[row][1] = o.getClientId();
            data[row][2] = o.getDate();
            data[row][3] = o.getPrice();
            row++;
        }
        jTable = new JTable(data, columns);
        JScrollPane jsp = new JScrollPane(jTable);
        employeeView.addTable(jsp);
    }

    private void addProduct() {
        if (jTable != null) {
            int[] selected = jTable.getSelectedRows();
            if (selected.length < 2) {//base product
                JPanel myPanel = new JPanel();
                JTextField titleField = new JTextField(5);
                JTextField ratingField = new JTextField(5);
                JTextField caloriesField = new JTextField(5);
                JTextField proteinField = new JTextField(5);
                JTextField fatField = new JTextField(5);
                JTextField sodiumField = new JTextField(5);
                JTextField priceField = new JTextField(5);
                myPanel.add(new JLabel("Title:"));
                myPanel.add(titleField);
                myPanel.add(new JLabel("Rating:"));
                myPanel.add(ratingField);
                myPanel.add(new JLabel("Calories:"));
                myPanel.add(caloriesField);
                myPanel.add(new JLabel("Protein:"));
                myPanel.add(proteinField);
                myPanel.add(new JLabel("Fat:"));
                myPanel.add(fatField);
                myPanel.add(new JLabel("Sodium:"));
                myPanel.add(sodiumField);
                myPanel.add(new JLabel("Price:"));
                myPanel.add(priceField);
                int result = JOptionPane.showConfirmDialog(null, myPanel, "Base Product", JOptionPane.OK_CANCEL_OPTION);
                if (result == JOptionPane.OK_OPTION) {
                    try {
                        MenuItem newItem = new BaseProduct(titleField.getText(), Float.parseFloat(ratingField.getText()), Integer.parseInt(caloriesField.getText()), Integer.parseInt(proteinField.getText()), Integer.parseInt(fatField.getText()), Integer.parseInt(sodiumField.getText()), Integer.parseInt(priceField.getText()));
                        deliveryService.addProduct(newItem);
                        JOptionPane.showMessageDialog(null, "Product Added Successfully!" + '\n' + newItem.toString());
                    } catch (NumberFormatException nfe) {
                        JOptionPane.showMessageDialog(null, "Bad Input!");
                    }
                }
            } else {//composite product
                List<MenuItem> composition = new LinkedList<>();
                for (int j : selected) {
                    composition.add(new BaseProduct(jTable.getValueAt(j, 0).toString(),
                            Float.parseFloat(jTable.getValueAt(j, 1).toString()),
                            Integer.parseInt(jTable.getValueAt(j, 2).toString()),
                            Integer.parseInt(jTable.getValueAt(j, 3).toString()),
                            Integer.parseInt(jTable.getValueAt(j, 4).toString()),
                            Integer.parseInt(jTable.getValueAt(j, 5).toString()),
                            Integer.parseInt(jTable.getValueAt(j, 6).toString())));
                }
                StringBuilder message = new StringBuilder();
                for (MenuItem m : composition) {
                    message.append(m.toString()).append('\n');
                }
                int result = JOptionPane.showConfirmDialog(null, "Product Will Contain:\n" + message, "Composite Product", JOptionPane.OK_CANCEL_OPTION);
                if (result == JOptionPane.OK_OPTION) {
                    JPanel myPanel = new JPanel();
                    JTextField titleField = new JTextField(5);
                    JTextField ratingField = new JTextField(5);
                    myPanel.add(new JLabel("Title:"));
                    myPanel.add(titleField);
                    myPanel.add(new JLabel("Rating:"));
                    myPanel.add(ratingField);
                    int result2 = JOptionPane.showConfirmDialog(null, myPanel, "Composite Product", JOptionPane.OK_CANCEL_OPTION);
                    if (result2 == JOptionPane.OK_OPTION) {
                        try {
                            MenuItem newCompositeProduct = new CompositeProduct(titleField.getText(), Float.parseFloat(ratingField.getText()), composition);
                            deliveryService.addProduct(newCompositeProduct);
                            JOptionPane.showMessageDialog(null, "Product Added Successfully\n" + newCompositeProduct.toString());
                        } catch (NumberFormatException nfe) {
                            JOptionPane.showMessageDialog(null, "Bad Input!");
                        }
                    }
                }
            }
        }
    }

    private void createUser(SignUpView signUpView, UserType userType) {
        if (signUpView.getUsername().equals("")) {
            JOptionPane.showMessageDialog(signUpView, "Invalid username!");
            return;
        }
        if (signUpView.getPassword().equals("")) {
            JOptionPane.showMessageDialog(signUpView, "Invalid password!");
            return;
        }
        if (!signUpView.getPassword().equals(signUpView.getConfirmedPassword())) {
            JOptionPane.showMessageDialog(signUpView, "Password does not match!");
            return;
        }
        if (!signUpView.getEmail().contains("@") || !signUpView.getEmail().contains(".")) {
            JOptionPane.showMessageDialog(signUpView, "Invalid email");
            return;
        }
        User newUser = new User(signUpView.getUsername(), signUpView.getPassword(), userType, signUpView.getEmail());
        JOptionPane.showMessageDialog(signUpView, "Account created successfully!");
        Set<User> allUsers = Serializator.deserializeUsers();
        for (User u : allUsers) {
            if (u.getUsername().equals(newUser.getUsername())) {
                JOptionPane.showMessageDialog(signUpView, "This username already exists!");
                return;
            }
            if (u.getEmail().equals(newUser.getEmail())) {
                JOptionPane.showMessageDialog(signUpView, "This email already exists!");
                return;
            }
        }
        allUsers.add(newUser);
        Serializator.serializeUsers(allUsers);
    }

    private int getMinValue(String string) {
        int minValue;
        if (string.equals(""))
            minValue = 0;
        else
            minValue = Integer.parseInt(string);
        return minValue;
    }

    private int getMaxValue(String string) {
        int maxValue;
        if (string.equals(""))
            maxValue = Integer.MAX_VALUE;
        else
            maxValue = Integer.parseInt(string);
        return maxValue;
    }

    private Order findOrderById(int id) {
        for (Order o : unfinishedOrders.keySet()) {
            if (o.getOrderId() == id) {
                return o;
            }
        }
        return null;
    }
}
