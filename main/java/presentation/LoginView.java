package presentation;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

public class LoginView extends JFrame {
    private final JTextField textFieldUsername;
    private final JTextField textFieldPassword;
    private final JButton signupButton;
    private final JButton loginButton;

    public LoginView() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(0, 0, 800, 449);
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel welcomeLabel = new JLabel("Welcome");
        welcomeLabel.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 50));
        welcomeLabel.setBackground(Color.WHITE);
        welcomeLabel.setForeground(Color.WHITE);
        welcomeLabel.setBounds(470, 87, 226, 95);
        contentPane.add(welcomeLabel);

        textFieldUsername = new JTextField("");
        textFieldUsername.setToolTipText("Enter username");
        textFieldUsername.setFont(new Font("Tahoma", Font.PLAIN, 20));
        textFieldUsername.setBounds(470, 203, 226, 35);
        contentPane.add(textFieldUsername);
        textFieldUsername.setColumns(10);

        textFieldPassword = new JPasswordField();
        textFieldPassword.setToolTipText("Enter password");
        textFieldPassword.setFont(new Font("Tahoma", Font.PLAIN, 20));
        textFieldPassword.setBounds(470, 264, 226, 35);
        contentPane.add(textFieldPassword);
        textFieldPassword.setColumns(10);

        JLabel usernameLabel = new JLabel("Username");
        usernameLabel.setFont(new Font("Tempus Sans ITC", Font.BOLD, 30));
        usernameLabel.setForeground(Color.WHITE);
        usernameLabel.setBounds(314, 203, 142, 35);
        contentPane.add(usernameLabel);

        JLabel passwordLabel = new JLabel("Password");
        passwordLabel.setFont(new Font("Tempus Sans ITC", Font.BOLD, 30));
        passwordLabel.setForeground(Color.WHITE);
        passwordLabel.setBounds(314, 264, 142, 35);
        contentPane.add(passwordLabel);

        loginButton = new JButton("Log in");
        loginButton.setBackground(new Color(78, 53, 36));
        loginButton.setForeground(Color.WHITE);
        loginButton.setFont(new Font("Tempus Sans ITC", Font.BOLD, 20));
        loginButton.setBounds(587, 309, 109, 35);
        contentPane.add(loginButton);

        signupButton = new JButton("Sign up");
        signupButton.setFont(new Font("Tempus Sans ITC", Font.BOLD, 20));
        signupButton.setBackground(new Color(78, 53, 36));
        signupButton.setForeground(Color.WHITE);
        signupButton.setBounds(470, 309, 109, 35);
        contentPane.add(signupButton);

        try {
            JLabel photoLabel = new JLabel(new ImageIcon(ImageIO.read(new File("src\\main\\resources\\LoginBackgroundLarge.jpg"))));
            photoLabel.setBounds(0, 0, 800, 449);
            contentPane.add(photoLabel);
        } catch (IOException e) {
            e.printStackTrace();
        }


        this.setVisible(true);
    }

    public void loginButtonListener(ActionListener actionListener) {
        this.loginButton.addActionListener(actionListener);
    }

    public void signupButtonListener(ActionListener actionListener) {
        this.signupButton.addActionListener(actionListener);
    }

    public String getUsername() {
        return this.textFieldUsername.getText();
    }

    public String getPassword() {
        return this.textFieldPassword.getText();
    }
}
