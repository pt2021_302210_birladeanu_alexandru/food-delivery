package presentation;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

public class ClientView extends JFrame {
    private final JPanel contentPane;
    private final JButton btnViewProducts;
    private final JButton btnAddToCart;
    private final JButton btnEmptyCart;
    private final JButton btnOrder;
    private JScrollPane scrollPane;
    private final JTextField textFieldMinPrice;
    private final JTextField textFieldMaxPrice;
    private final JTextField textFieldMinSodium;
    private final JTextField textFieldMaxSodium;
    private final JTextField textFieldMinFat;
    private final JTextField textFieldMaxFat;
    private final JTextField textFieldMinProtein;
    private final JTextField textFieldMaxProtein;
    private final JTextField textFieldMinCalories;
    private final JTextField textFieldMaxCalories;
    private final JTextField textFieldMinRating;
    private final JTextField textFieldMaxRating;
    private final JTextField textFieldTitle;

    public ClientView() {

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(0, 0, 800, 449);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        btnViewProducts = new JButton("View Products");
        btnViewProducts.setHorizontalAlignment(SwingConstants.LEFT);
        btnViewProducts.setFont(new Font("Tempus Sans ITC", Font.BOLD, 18));
        btnViewProducts.setForeground(Color.WHITE);
        btnViewProducts.setBackground(new Color(4, 132, 204));
        //btnViewProducts.setBackground(Color.BLACK);
        btnViewProducts.setBounds(10, 348, 155, 39);
        contentPane.add(btnViewProducts);

        textFieldMinPrice = new JTextField("");
        textFieldMinPrice.setToolTipText("MIN");
        textFieldMinPrice.setBounds(73, 319, 41, 19);
        contentPane.add(textFieldMinPrice);
        textFieldMinPrice.setColumns(10);
        JLabel lblNewLabel_1 = new JLabel("Price");
        lblNewLabel_1.setFont(new Font("Tempus Sans ITC", Font.BOLD, 15));
        lblNewLabel_1.setForeground(Color.WHITE);
        lblNewLabel_1.setBounds(10, 319, 54, 19);
        contentPane.add(lblNewLabel_1);
        textFieldMaxPrice = new JTextField("");
        textFieldMaxPrice.setToolTipText("MAX");
        textFieldMaxPrice.setBounds(124, 319, 41, 19);
        contentPane.add(textFieldMaxPrice);
        textFieldMaxPrice.setColumns(10);

        textFieldMinSodium = new JTextField("");
        textFieldMinSodium.setToolTipText("MIN");
        textFieldMinSodium.setBounds(73, 289, 41, 19);
        contentPane.add(textFieldMinSodium);
        textFieldMinSodium.setColumns(10);
        JLabel lblNewLabel_2 = new JLabel("Sodium");
        lblNewLabel_2.setFont(new Font("Tempus Sans ITC", Font.BOLD, 15));
        lblNewLabel_2.setForeground(Color.WHITE);
        lblNewLabel_2.setBounds(10, 289, 54, 19);
        contentPane.add(lblNewLabel_2);
        textFieldMaxSodium = new JTextField("");
        textFieldMaxSodium.setToolTipText("MAX");
        textFieldMaxSodium.setBounds(124, 289, 41, 19);
        contentPane.add(textFieldMaxSodium);
        textFieldMaxSodium.setColumns(10);

        textFieldMinFat = new JTextField("");
        textFieldMinFat.setToolTipText("MIN");
        textFieldMinFat.setBounds(73, 259, 41, 19);
        contentPane.add(textFieldMinFat);
        textFieldMinFat.setColumns(10);
        JLabel lblNewLabel_3 = new JLabel("Fat");
        lblNewLabel_3.setFont(new Font("Tempus Sans ITC", Font.BOLD, 15));
        lblNewLabel_3.setForeground(Color.WHITE);
        lblNewLabel_3.setBounds(10, 259, 54, 19);
        contentPane.add(lblNewLabel_3);
        textFieldMaxFat = new JTextField("");
        textFieldMaxFat.setToolTipText("MAX");
        textFieldMaxFat.setBounds(124, 259, 41, 19);
        contentPane.add(textFieldMaxFat);
        textFieldMaxFat.setColumns(10);

        textFieldMinProtein = new JTextField("");
        textFieldMinProtein.setToolTipText("MIN");
        textFieldMinProtein.setBounds(73, 229, 41, 19);
        contentPane.add(textFieldMinProtein);
        textFieldMinProtein.setColumns(10);
        JLabel lblNewLabel_4 = new JLabel("Protein");
        lblNewLabel_4.setFont(new Font("Tempus Sans ITC", Font.BOLD, 15));
        lblNewLabel_4.setForeground(Color.WHITE);
        lblNewLabel_4.setBounds(10, 229, 54, 19);
        contentPane.add(lblNewLabel_4);
        textFieldMaxProtein = new JTextField("");
        textFieldMaxProtein.setToolTipText("MAX");
        textFieldMaxProtein.setBounds(124, 229, 41, 19);
        contentPane.add(textFieldMaxProtein);
        textFieldMaxProtein.setColumns(10);

        textFieldMinCalories = new JTextField("");
        textFieldMinCalories.setToolTipText("MIN");
        textFieldMinCalories.setBounds(73, 199, 41, 19);
        contentPane.add(textFieldMinCalories);
        textFieldMinCalories.setColumns(10);
        JLabel lblNewLabel_5 = new JLabel("Calories");
        lblNewLabel_5.setFont(new Font("Tempus Sans ITC", Font.BOLD, 14));
        lblNewLabel_5.setForeground(Color.WHITE);
        lblNewLabel_5.setBounds(10, 199, 54, 19);
        contentPane.add(lblNewLabel_5);
        textFieldMaxCalories = new JTextField("");
        textFieldMaxCalories.setToolTipText("MAX");
        textFieldMaxCalories.setBounds(124, 199, 41, 19);
        contentPane.add(textFieldMaxCalories);
        textFieldMaxCalories.setColumns(10);

        textFieldMinRating = new JTextField("");
        textFieldMinRating.setToolTipText("MIN");
        textFieldMinRating.setBounds(73, 169, 41, 19);
        contentPane.add(textFieldMinRating);
        textFieldMinRating.setColumns(10);
        JLabel lblNewLabel_6 = new JLabel("Rating");
        lblNewLabel_6.setFont(new Font("Tempus Sans ITC", Font.BOLD, 15));
        lblNewLabel_6.setForeground(Color.WHITE);
        lblNewLabel_6.setBounds(10, 169, 54, 19);
        contentPane.add(lblNewLabel_6);
        textFieldMaxRating = new JTextField("");
        textFieldMaxRating.setToolTipText("MAX");
        textFieldMaxRating.setBounds(124, 169, 41, 19);
        contentPane.add(textFieldMaxRating);
        textFieldMaxRating.setColumns(10);

        JLabel lblNewLabel_7 = new JLabel("Title");
        lblNewLabel_7.setFont(new Font("Tempus Sans ITC", Font.BOLD, 15));
        lblNewLabel_7.setForeground(Color.WHITE);
        lblNewLabel_7.setBounds(10, 139, 54, 19);
        contentPane.add(lblNewLabel_7);
        textFieldTitle = new JTextField("");
        textFieldTitle.setBounds(73, 139, 92, 19);
        contentPane.add(textFieldTitle);
        textFieldTitle.setColumns(10);

        btnAddToCart = new JButton("Add to Cart");
        btnAddToCart.setHorizontalAlignment(SwingConstants.CENTER);
        btnAddToCart.setFont(new Font("Tempus Sans ITC", Font.BOLD, 15));
        btnAddToCart.setForeground(Color.WHITE);
        btnAddToCart.setBackground(new Color(4, 132, 204));
        btnAddToCart.setBounds(650, 142, 125, 39);
        contentPane.add(btnAddToCart);

        btnEmptyCart = new JButton("Empty Cart");
        btnEmptyCart.setHorizontalAlignment(SwingConstants.CENTER);
        btnEmptyCart.setFont(new Font("Tempus Sans ITC", Font.BOLD, 15));
        btnEmptyCart.setForeground(Color.WHITE);
        btnEmptyCart.setBackground(new Color(4, 132, 204));
        btnEmptyCart.setBounds(650, 192, 125, 39);
        contentPane.add(btnEmptyCart);

        btnOrder = new JButton("Order");
        btnOrder.setHorizontalAlignment(SwingConstants.CENTER);
        btnOrder.setFont(new Font("Tempus Sans ITC", Font.BOLD, 15));
        btnOrder.setForeground(Color.WHITE);
        btnOrder.setBackground(new Color(4, 132, 204));
        btnOrder.setBounds(650, 242, 125, 39);
        contentPane.add(btnOrder);

        try {
            JLabel photoLabel = new JLabel(new ImageIcon(ImageIO.read(new File("src\\main\\resources\\ClientBackground.jpeg"))));
            photoLabel.setBounds(0, 0, 800, 449);
            contentPane.add(photoLabel);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.setVisible(true);
    }

    public void viewProductsButtonListener(ActionListener actionListener) {
        this.btnViewProducts.addActionListener(actionListener);
    }

    public void addToCartButtonListener(ActionListener actionListener) {
        this.btnAddToCart.addActionListener(actionListener);
    }

    public void emptyCartButtonListener(ActionListener actionListener) {
        this.btnEmptyCart.addActionListener(actionListener);
    }

    public void orderButtonListener(ActionListener actionListener) {
        this.btnOrder.addActionListener(actionListener);
    }

    public String getTitle() {
        return textFieldTitle.getText();
    }

    public String getMinRating() {
        return textFieldMinRating.getText();
    }

    public String getMaxRating() {
        return textFieldMaxRating.getText();
    }

    public String getMinCalories() {
        return textFieldMinCalories.getText();
    }

    public String getMaxCalories() {
        return textFieldMaxCalories.getText();
    }

    public String getMinProtein() {
        return textFieldMinProtein.getText();
    }

    public String getMaxProtein() {
        return textFieldMaxProtein.getText();
    }

    public String getMinFat() {
        return textFieldMinFat.getText();
    }

    public String getMaxFat() {
        return textFieldMaxFat.getText();
    }

    public String getMinSodium() {
        return textFieldMinSodium.getText();
    }

    public String getMaxSodium() {
        return textFieldMaxSodium.getText();
    }

    public String getMinPrice() {
        return textFieldMinPrice.getText();
    }

    public String getMaxPrice() {
        return textFieldMaxPrice.getText();
    }

    public void addTable(JScrollPane scrollPane) {
        if (this.scrollPane != null)
            contentPane.remove(this.scrollPane);
        this.scrollPane = scrollPane;
        scrollPane.setBounds(219, 46, 359, 356);
        contentPane.add(scrollPane);
    }
}
