package presentation;

import businessLogic.DeliveryService;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

public class ReportView extends JFrame implements ActionListener {
    private final JTextField textFieldStartHour;
    private final JTextField textFieldEndHour;
    private final JTextField textFieldReport2;
    private final JTextField textFieldReport3Times;
    private final JTextField textFieldReport3Price;
    private final JTextField textFieldReport4Day;
    private final JRadioButton rdbtn1;
    private final JRadioButton rdbtn2;
    private final JRadioButton rdbtn3;
    private final JRadioButton rdbtn4;
    private final JLabel lblNewLabel;
    private final JLabel lblNewLabel_1;
    private final JLabel lblNewLabel_2;
    private final JLabel lblNewLabel_3;
    private final JLabel lblNewLabel_4;
    private final JLabel lblNewLabel_5;
    private final JButton btnWriteReport;

    public ReportView() {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(0, 0, 800, 449);
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        rdbtn1 = new JRadioButton("Orders Performed Between Given Hours");
        rdbtn1.setFont(new Font("Tempus Sans ITC", Font.BOLD, 20));
        rdbtn1.setBounds(30, 40, 401, 41);
        contentPane.add(rdbtn1);

        rdbtn2 = new JRadioButton("Products Ordered More Than a Given Number of Times");
        rdbtn2.setFont(new Font("Tempus Sans ITC", Font.BOLD, 20));
        rdbtn2.setBounds(30, 111, 534, 41);
        contentPane.add(rdbtn2);

        rdbtn3 = new JRadioButton("Clients Who Ordered More Than a Given Number of Times");
        rdbtn3.setFont(new Font("Tempus Sans ITC", Font.BOLD, 20));
        rdbtn3.setBounds(30, 182, 559, 41);
        contentPane.add(rdbtn3);

        rdbtn4 = new JRadioButton("Products Ordered on a Given Day");
        rdbtn4.setFont(new Font("Tempus Sans ITC", Font.BOLD, 20));
        rdbtn4.setBounds(30, 253, 344, 41);
        contentPane.add(rdbtn4);

        lblNewLabel = new JLabel("Start Hour:");
        lblNewLabel.setFont(new Font("Tempus Sans ITC", Font.BOLD, 15));
        lblNewLabel.setBounds(40, 80, 83, 25);
        contentPane.add(lblNewLabel);

        textFieldStartHour = new JTextField();
        textFieldStartHour.setBounds(120, 81, 58, 25);
        contentPane.add(textFieldStartHour);
        textFieldStartHour.setColumns(10);

        lblNewLabel_1 = new JLabel("End Hour:");
        lblNewLabel_1.setFont(new Font("Tempus Sans ITC", Font.BOLD, 15));
        lblNewLabel_1.setBounds(207, 83, 76, 18);
        contentPane.add(lblNewLabel_1);

        textFieldEndHour = new JTextField();
        textFieldEndHour.setBounds(284, 81, 58, 25);
        contentPane.add(textFieldEndHour);
        textFieldEndHour.setColumns(10);

        lblNewLabel_2 = new JLabel("Times Ordered:");
        lblNewLabel_2.setFont(new Font("Tempus Sans ITC", Font.BOLD, 15));
        lblNewLabel_2.setBounds(40, 151, 113, 25);
        contentPane.add(lblNewLabel_2);

        textFieldReport2 = new JTextField();
        textFieldReport2.setBounds(151, 151, 58, 25);
        contentPane.add(textFieldReport2);
        textFieldReport2.setColumns(10);

        lblNewLabel_3 = new JLabel("Day:");
        lblNewLabel_3.setFont(new Font("Tempus Sans ITC", Font.BOLD, 15));
        lblNewLabel_3.setBounds(40, 293, 44, 25);
        contentPane.add(lblNewLabel_3);

        lblNewLabel_4 = new JLabel("Times Ordered:");
        lblNewLabel_4.setFont(new Font("Tempus Sans ITC", Font.BOLD, 15));
        lblNewLabel_4.setBounds(40, 222, 113, 25);
        contentPane.add(lblNewLabel_4);

        textFieldReport3Times = new JTextField();
        textFieldReport3Times.setBounds(151, 223, 58, 25);
        contentPane.add(textFieldReport3Times);
        textFieldReport3Times.setColumns(10);

        lblNewLabel_5 = new JLabel("Order Cost Greater Than:");
        lblNewLabel_5.setFont(new Font("Tempus Sans ITC", Font.BOLD, 15));
        lblNewLabel_5.setBounds(232, 222, 178, 25);
        contentPane.add(lblNewLabel_5);

        textFieldReport3Price = new JTextField();
        textFieldReport3Price.setBounds(411, 223, 58, 25);
        contentPane.add(textFieldReport3Price);
        textFieldReport3Price.setColumns(10);

        textFieldReport4Day = new JTextField();
        textFieldReport4Day.setBounds(80, 294, 58, 25);
        contentPane.add(textFieldReport4Day);
        textFieldReport4Day.setColumns(10);

        btnWriteReport = new JButton("Write Report");
        btnWriteReport.setFont(new Font("Tempus Sans ITC", Font.BOLD, 20));
        btnWriteReport.setBounds(411, 324, 184, 32);
        contentPane.add(btnWriteReport);

        ButtonGroup g = new ButtonGroup();
        g.add(rdbtn1);
        g.add(rdbtn2);
        g.add(rdbtn3);
        g.add(rdbtn4);

        rdbtn1.addActionListener(this);
        rdbtn2.addActionListener(this);
        rdbtn3.addActionListener(this);
        rdbtn4.addActionListener(this);

        lblNewLabel.setVisible(false);
        textFieldStartHour.setVisible(false);
        lblNewLabel_1.setVisible(false);
        textFieldEndHour.setVisible(false);
        lblNewLabel_2.setVisible(false);
        textFieldReport2.setVisible(false);
        lblNewLabel_4.setVisible(false);
        lblNewLabel_5.setVisible(false);
        textFieldReport3Times.setVisible(false);
        textFieldReport3Price.setVisible(false);
        lblNewLabel_3.setVisible(false);
        textFieldReport4Day.setVisible(false);

        try {
            JLabel photoLabel = new JLabel(new ImageIcon(ImageIO.read(new File("src\\main\\resources\\ReportBackground.jpg"))));
            photoLabel.setBounds(0, 0, 800, 449);
            contentPane.add(photoLabel);
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (rdbtn1.isSelected()) {
            lblNewLabel.setVisible(true);
            textFieldStartHour.setVisible(true);
            lblNewLabel_1.setVisible(true);
            textFieldEndHour.setVisible(true);
        } else {
            lblNewLabel.setVisible(false);
            textFieldStartHour.setVisible(false);
            lblNewLabel_1.setVisible(false);
            textFieldEndHour.setVisible(false);
        }
        if (rdbtn2.isSelected()) {
            lblNewLabel_2.setVisible(true);
            textFieldReport2.setVisible(true);
        } else {
            lblNewLabel_2.setVisible(false);
            textFieldReport2.setVisible(false);
        }
        if (rdbtn3.isSelected()) {
            lblNewLabel_4.setVisible(true);
            lblNewLabel_5.setVisible(true);
            textFieldReport3Times.setVisible(true);
            textFieldReport3Price.setVisible(true);
        } else {
            lblNewLabel_4.setVisible(false);
            lblNewLabel_5.setVisible(false);
            textFieldReport3Times.setVisible(false);
            textFieldReport3Price.setVisible(false);
        }
        if (rdbtn4.isSelected()) {
            lblNewLabel_3.setVisible(true);
            textFieldReport4Day.setVisible(true);
        } else {
            lblNewLabel_3.setVisible(false);
            textFieldReport4Day.setVisible(false);
        }

        btnWriteReport.addActionListener(e1 -> {
            DeliveryService deliveryService = new DeliveryService();
            if (rdbtn1.isSelected()) {
                try {
                    int startHour = Integer.parseInt(textFieldStartHour.getText());
                    int endHour = Integer.parseInt(textFieldEndHour.getText());
                    String message = deliveryService.generateReport1(startHour, endHour);
                    JOptionPane.showMessageDialog(null, message);
                } catch (NumberFormatException nfe) {
                    JOptionPane.showMessageDialog(null, "Bad input!");
                }
            }
            if (rdbtn2.isSelected()) {
                try {
                    int timesOrdered = Integer.parseInt(textFieldReport2.getText());
                    String message = deliveryService.generateReport2(timesOrdered);
                    JOptionPane.showMessageDialog(null, message);
                } catch (NumberFormatException nfe) {
                    JOptionPane.showMessageDialog(null, "Bad input!");
                }
            }
            if (rdbtn3.isSelected()) {
                try {
                    int timesOrdered = Integer.parseInt(textFieldReport3Times.getText());
                    int lowPriceBound = Integer.parseInt(textFieldReport3Price.getText());
                    String message = deliveryService.generateReport3(timesOrdered, lowPriceBound);
                    JOptionPane.showMessageDialog(null, message);
                } catch (NumberFormatException nfe) {
                    JOptionPane.showMessageDialog(null, "Bad input!");
                }
            }
            if (rdbtn4.isSelected()) {
                try {
                    int day = Integer.parseInt(textFieldReport4Day.getText());
                    String message = deliveryService.generateReport4(day);
                    JOptionPane.showMessageDialog(null, message);
                } catch (NumberFormatException nfe) {
                    JOptionPane.showMessageDialog(null, "Bad input!");
                }
            }
            this.dispose();
        });
    }
}
