package presentation;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

public class AdminView extends JFrame {
    private final JPanel contentPane;
    private final JButton btnViewProducts;
    private final JButton btnAddStaffAccount;
    private final JButton btnImportProducts;
    private final JButton btnDeleteProduct;
    private final JButton btnSearchByName;
    private final JButton btnGenerateReport;
    private final JButton btnSaveChanges;
    private final JButton btnAddProduct;
    private JScrollPane scrollPane;
    private final JTextField textFieldSearch;

    public AdminView() {
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(0, 0, 800, 449);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel lblNewLabel = new JLabel("Search");
        lblNewLabel.setForeground(Color.WHITE);
        lblNewLabel.setFont(new Font("Tempus Sans ITC", Font.BOLD, 20));
        lblNewLabel.setBounds(262, 15, 81, 31);
        contentPane.add(lblNewLabel);

        textFieldSearch = new JTextField();
        textFieldSearch.setBounds(350, 15, 310, 31);
        contentPane.add(textFieldSearch);
        textFieldSearch.setColumns(10);

        btnViewProducts = new JButton("View all Products");
        btnViewProducts.setFont(new Font("Tempus Sans ITC", Font.BOLD, 20));
        btnViewProducts.setForeground(Color.WHITE);
        btnViewProducts.setBackground(new Color(44, 44, 44));
        btnViewProducts.setBounds(25, 15, 205, 39);
        contentPane.add(btnViewProducts);

        btnAddStaffAccount = new JButton("Add Staff Account");
        btnAddStaffAccount.setFont(new Font("Tempus Sans ITC", Font.BOLD, 20));
        btnAddStaffAccount.setForeground(Color.WHITE);
        btnAddStaffAccount.setBackground(new Color(44, 44, 44));
        btnAddStaffAccount.setBounds(25, 365, 205, 39);
        contentPane.add(btnAddStaffAccount);

        btnSearchByName = new JButton("Search Products");
        btnSearchByName.setFont(new Font("Tempus Sans ITC", Font.BOLD, 20));
        btnSearchByName.setForeground(Color.WHITE);
        btnSearchByName.setBackground(new Color(44, 44, 44));
        btnSearchByName.setBounds(25, 65, 205, 39);
        contentPane.add(btnSearchByName);

        btnDeleteProduct = new JButton("Delete Product");
        btnDeleteProduct.setFont(new Font("Tempus Sans ITC", Font.BOLD, 20));
        btnDeleteProduct.setForeground(Color.WHITE);
        btnDeleteProduct.setBackground(new Color(44, 44, 44));
        btnDeleteProduct.setBounds(25, 165, 205, 39);
        contentPane.add(btnDeleteProduct);

        btnImportProducts = new JButton("Import Products");
        btnImportProducts.setFont(new Font("Tempus Sans ITC", Font.BOLD, 20));
        btnImportProducts.setForeground(Color.WHITE);
        btnImportProducts.setBackground(new Color(44, 44, 44));
        btnImportProducts.setBounds(25, 215, 205, 39);
        contentPane.add(btnImportProducts);

        btnGenerateReport = new JButton("Generate a Report");
        btnGenerateReport.setFont(new Font("Tempus Sans ITC", Font.BOLD, 20));
        btnGenerateReport.setForeground(Color.WHITE);
        btnGenerateReport.setBackground(new Color(44, 44, 44));
        btnGenerateReport.setBounds(25, 265, 205, 39);
        contentPane.add(btnGenerateReport);

        btnSaveChanges = new JButton("Save Changes");
        btnSaveChanges.setFont(new Font("Tempus Sans ITC", Font.BOLD, 20));
        btnSaveChanges.setForeground(Color.WHITE);
        btnSaveChanges.setBackground(new Color(44, 44, 44));
        btnSaveChanges.setBounds(25, 315, 205, 39);
        contentPane.add(btnSaveChanges);

        btnAddProduct = new JButton("Add Product");
        btnAddProduct.setFont(new Font("Tempus Sans ITC", Font.BOLD, 20));
        btnAddProduct.setForeground(Color.WHITE);
        btnAddProduct.setBackground(new Color(44, 44, 44));
        btnAddProduct.setBounds(25, 115, 205, 39);
        contentPane.add(btnAddProduct);

        try {
            JLabel photoLabel = new JLabel(new ImageIcon(ImageIO.read(new File("src\\main\\resources\\AdminBackground.jpg"))));
            photoLabel.setBounds(0, 0, 800, 449);
            contentPane.add(photoLabel);
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.setVisible(true);
    }

    public void viewProductsButtonListener(ActionListener actionListener) {
        this.btnViewProducts.addActionListener(actionListener);
    }

    public void addStaffAccountButtonListener(ActionListener actionListener) {
        this.btnAddStaffAccount.addActionListener(actionListener);
    }

    public void importProductsButtonListener(ActionListener actionListener) {
        this.btnImportProducts.addActionListener(actionListener);
    }

    public void deleteProductButtonListener(ActionListener actionListener) {
        this.btnDeleteProduct.addActionListener(actionListener);
    }

    public void searchByNameButtonListener(ActionListener actionListener) {
        this.btnSearchByName.addActionListener(actionListener);
    }

    public void generateReportButtonListener(ActionListener actionListener) {
        this.btnGenerateReport.addActionListener(actionListener);
    }

    public void saveChangesButtonListener(ActionListener actionListener) {
        this.btnSaveChanges.addActionListener(actionListener);
    }

    public void addProductButtonListener(ActionListener actionListener) {
        this.btnAddProduct.addActionListener(actionListener);
    }

    public void addTable(JScrollPane scrollPane) {
        if (this.scrollPane != null)
            contentPane.remove(this.scrollPane);
        this.scrollPane = scrollPane;
        scrollPane.setBounds(250, 65, 500, 339);
        contentPane.add(scrollPane);
    }

    public String getTextFieldSearch() {
        return this.textFieldSearch.getText();
    }


   /* public void dispose() {
        new LoginView();
        super.dispose();
    }*/
}
