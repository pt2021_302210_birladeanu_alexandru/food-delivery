Order{orderId=4, clientId=3, date=2021-05-24T20:41:01.661203700, price=806}
[
MenuItem{title='Surfer's Granola', rating=2.5, calories=303, protein=8, fat=16, sodium=157, price=24}, 
MenuItem{title='Chocolate Raspberry Roulade', rating=3.75, calories=217, protein=5, fat=5, sodium=165, price=26}, 
MenuItem{title='Pear Cake with Lemon-Honey Cream Cheese Frosting', rating=4.375, calories=839, protein=59, fat=16, sodium=932, price=11}, 
MenuItem{title='Apple Pancakes with Cinnamon Butter', rating=4.375, calories=146, protein=2, fat=9, sodium=106, price=50}, 
MenuItem{title='Oil and Vinegar Potato Salad', rating=3.75, calories=259, protein=4, fat=14, sodium=13, price=27}, 
MenuItem{title='Croque-Monsieur', rating=0.0, calories=794, protein=63, fat=40, sodium=534, price=58}, 
MenuItem{title='Chicken Sauté with Sherry', rating=3.75, calories=1012, protein=86, fat=37, sodium=1994, price=31}, 
MenuItem{title='Melissa Hotek's Granola', rating=4.375, calories=337, protein=10, fat=17, sodium=7, price=43}, 
MenuItem{title='Cheese Blintzes with Blueberry Sauce', rating=4.375, calories=261, protein=9, fat=11, sodium=402, price=79}, 
MenuItem{title='Boulevardier', rating=0.0, calories=231, protein=0, fat=0, sodium=2, price=95}, 
MenuItem{title='Chicken Fricassée with Creamy Sweet-and-Sour Dill Sauce', rating=2.5, calories=678, protein=60, fat=39, sodium=936, price=20}, 
MenuItem{title='Roasted Guinea Hens with Whole-Grain Mustard and Herbs', rating=4.375, calories=1193, protein=95, fat=65, sodium=853, price=38}, 
MenuItem{title='Apricot and Pistachio Baked Rice', rating=3.75, calories=239, protein=4, fat=6, sodium=1442, price=96}, 
MenuItem{title='No-Bake Chocolate and Cream Cheese Pie', rating=3.75, calories=520, protein=5, fat=34, sodium=53, price=34}, 
MenuItem{title='Port-Currant Sauce', rating=0.0, calories=1215, protein=18, fat=4, sodium=5302, price=17}, 
MenuItem{title='Pork Fricassée with Mushrooms and Carrots', rating=3.75, calories=727, protein=29, fat=28, sodium=374, price=63}, 
MenuItem{title='Peanut Butter Cheesecake with Peanut Brittle', rating=3.75, calories=320, protein=9, fat=25, sodium=146, price=94}]